#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/mutex.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/regulator/machine.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/spi/mcp23s08.h>
#include <linux/slab.h>
#include <asm/byteorder.h>
#include <plat/sys_config.h>

static int __init mcp23s08_board_init(void)
{
	const struct sunxi_section *sp;
	struct i2c_board_info *mcp_board_info;
	struct mcp23s08_platform_data *mcp_platform_data;

	int status = 0;
	int expander_count = 0;
	char gpio_expander_para_name[32];

	int device_used = -1;
	char name[I2C_NAME_SIZE];
	int twi_id = -1;
	__u32 twi_addr = 0;
	int base = -1;
	int pullups = -1;

	for (expander_count = 0; expander_count < 32; expander_count++) {
		snprintf(gpio_expander_para_name, sizeof(gpio_expander_para_name), "gpio_expander%d_para", expander_count);
		sp = sunxi_find_section(gpio_expander_para_name);
		if (!sp) {
			break;
		}
		pr_info("%s - read para section [%s]\n", __func__, gpio_expander_para_name);
		if (SCRIPT_PARSER_OK != script_parser_fetch(gpio_expander_para_name, "gpio_expander_used", &device_used, 1)) {
			pr_err("%s:%d - \"gpio_expander_used\" script_parser_fetch err\n", __func__,  __LINE__);
			continue;
		}
		if (device_used == 0) {
			continue;
		}
		memset(name, 0, sizeof(name));
		if (SCRIPT_PARSER_OK != script_parser_fetch(gpio_expander_para_name, "gpio_expander_name", (int *)name, sizeof(name))) {
			pr_err("%s:%d - \"gpio_expander_name\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(gpio_expander_para_name, "gpio_expander_twi_id", &twi_id, 1)) {
			pr_err("%s:%d - \"gpio_expander_twi_id\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(gpio_expander_para_name, "gpio_expander_twi_addr", &twi_addr, 1)) {
			pr_err("%s:%d - \"gpio_expander_twi_addr\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(gpio_expander_para_name, "gpio_expander_base", &base, 1)) {
			pr_err("%s:%d - \"gpio_expander_base\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(gpio_expander_para_name, "gpio_expander_pullups", &pullups, 1)) {
			pr_err("%s:%d - \"gpio_expander_pullups\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}

		mcp_board_info = kzalloc(sizeof(struct i2c_board_info), GFP_KERNEL);
		mcp_platform_data = kzalloc(sizeof(struct mcp23s08_platform_data), GFP_KERNEL);

		strlcpy(mcp_board_info->type, name, I2C_NAME_SIZE);
		mcp_board_info->addr = twi_addr;
		mcp_board_info->platform_data = mcp_platform_data;
		mcp_platform_data->chip[0].pullups = pullups;
		mcp_platform_data->base = base;

		status = i2c_register_board_info(twi_id, mcp_board_info, 1);
		if (status) {
			kfree(mcp_platform_data);
			kfree(mcp_board_info);
		}
	}
	return status;
}

fs_initcall(mcp23s08_board_init);

MODULE_DESCRIPTION("mcp23s08 board");
MODULE_AUTHOR("Flames Group / Ivan Metla");
MODULE_LICENSE("GPL");
