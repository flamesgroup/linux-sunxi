#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/mutex.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/regulator/machine.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/spi/mcp23s08.h>
#include <linux/slab.h>
#include <asm/byteorder.h>
#include <plat/sys_config.h>
#include "gpio-sunxi.h"

static int __init mcp23s08_fex_init(void)
{
	int i;
	int status = 0;
	int expander_count = 0;
	char gpio_expander_para_name[32];
	char gpio_expander_init_name[32];

	int device_used = -1;
	int base = -1;
	int direction_change = 0;

	const struct sunxi_section *sp;
	const struct sunxi_property *pp;
	unsigned int gpio_offset;
	unsigned int gpio;

	for (expander_count = 0; expander_count < 32; expander_count++) {
		snprintf(gpio_expander_para_name, sizeof(gpio_expander_para_name), "gpio_expander%d_para", expander_count);
		sp = sunxi_find_section(gpio_expander_para_name);
		if (!sp) {
			break;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(gpio_expander_para_name, "gpio_expander_used", &device_used, 1)) {
			pr_err("%s:%d - \"gpio_expander_used\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (device_used == 0) {
			continue;
		}

		if (SCRIPT_PARSER_OK != script_parser_fetch(gpio_expander_para_name, "gpio_expander_base", &base, 1)) {
			pr_err("%s:%d - \"gpio_expander_base\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}

		if (SCRIPT_PARSER_OK != script_parser_fetch(gpio_expander_para_name, "gpio_expander_direction_change", &direction_change, 1)) {
			pr_err("%s:%d - \"gpio_expander_dirrection_change\" script_parser_fetch err\n", __func__, __LINE__);
			direction_change = 0;
		}

		memset(gpio_expander_init_name, 0, sizeof(gpio_expander_init_name));
		snprintf(gpio_expander_init_name, sizeof(gpio_expander_init_name), "gpio_expander%d_init", expander_count);
		pr_info("%s - read init section [%s]\n", __func__, gpio_expander_init_name);
		sp = sunxi_find_section(gpio_expander_init_name);
		if (!sp) {
			continue;
		}

		sunxi_for_each_property(sp, pp, i) {
			if (SUNXI_PROP_TYPE_GPIO == sunxi_property_type(pp)) {
				const struct sunxi_property_gpio_value *gpio_configuration = sunxi_property_value(pp);
				/* magic 11 constant - start position of gpio offset number in gpio offset init name (example: pin_offset_15) */
				if (strlen(pp->name) < 11) {
					pr_err("%s:%d - incorrect length of gpio offset init name \"%s\"\n", __func__, __LINE__, pp->name);
					continue;
				}
				status = kstrtouint(pp->name + 11, 0, &gpio_offset);
				if (status < 0) {
					pr_err("%s:%d - can't get gpio offset number from gpio offset init name \"%s\"; err %d\n", __func__, __LINE__, pp->name, status);
					continue;
				}
				gpio = gpio_offset + base;
				status = gpio_request(gpio, "sysfs");
				if (status < 0) {
					pr_err("%s:%d - can't request gpio number %u; err %d\n", __func__, __LINE__, gpio, status);
					continue;
				}
				status = gpio_export(gpio, direction_change);
				if (status < 0) {
					gpio_free(gpio);
					pr_err("%s:%d - can't export gpio number %u; err %d\n", __func__, __LINE__, gpio, status);
					continue;
				}
				if (gpio_configuration->mul_sel == 0) {
					status = gpio_direction_input(gpio);
					if (status < 0) {
						gpio_free(gpio);
						pr_err("%s:%d - can't set gpio number %u to input direction; err %d\n", __func__, __LINE__, gpio, status);
						continue;
					}
				} else if (gpio_configuration->mul_sel == 1) {
					status = gpio_direction_output(gpio, gpio_configuration->data == 1 ? 1 : 0);
					if (status < 0) {
						gpio_free(gpio);
						pr_err("%s:%d - can't set gpio number %u to output direction; err %d\n", __func__, __LINE__, gpio, status);
						continue;
					}
				}
			}
		}
	}
	return status;
}

late_initcall(mcp23s08_fex_init);

MODULE_DESCRIPTION("mcp23s08 board");
MODULE_AUTHOR("Flames Group / Ivan Metla");
MODULE_LICENSE("GPL");
