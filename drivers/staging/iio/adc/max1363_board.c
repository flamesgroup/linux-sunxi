#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/mutex.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/regulator/machine.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/regulator/fixed.h>
#include <linux/slab.h>
#include <asm/byteorder.h>
#include <plat/sys_config.h>

static int __init max1363_board_init(void)
{
	int status = 0;
	int adc_count = 0;
	char adc_para_name[32];
	struct i2c_board_info *adc_board_info;

	struct regulator_consumer_supply *max1363_supplies;
	struct regulator_init_data *reg_init_data;
	struct fixed_voltage_config *reg_config;
	struct platform_device *vreg_dev;

	int device_used = -1;
	char name[I2C_NAME_SIZE];
	int twi_id = -1;
	__u32 twi_addr = 0;
	u32 supply_uv = 0;
	char *dev_name;
	const int dev_name_size = sizeof("x-xxxx");

	const struct sunxi_section *sp;

	for (adc_count = 0; adc_count < 8; adc_count++) {
		snprintf(adc_para_name, sizeof(adc_para_name), "iio_adc%d_para", adc_count);
		sp = sunxi_find_section(adc_para_name);
		if (!sp) {
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(adc_para_name, "iio_adc_used", &device_used, 1)) {
			pr_err("%s:%d - \"iio_adc_used\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (device_used == 0) {
			continue;
		}

		memset(name, 0, sizeof(name));
		if (SCRIPT_PARSER_OK != script_parser_fetch(adc_para_name, "iio_adc_name", (int *)name, sizeof(name))) {
			pr_err("%s:%d - \"iio_adc_name\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(adc_para_name, "iio_adc_twi_id", &twi_id, 1)) {
			pr_err("%s:%d - \"iio_adc_twi_id\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(adc_para_name, "iio_adc_twi_addr", &twi_addr, 1)) {
			pr_err("%s:%d - \"iio_adc_twi_addr\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(adc_para_name, "iio_adc_supply_uv", &supply_uv, 1)) {
			supply_uv = 3300000;
		}

		max1363_supplies = kzalloc(sizeof(struct regulator_consumer_supply), GFP_KERNEL);
		reg_init_data = kzalloc(sizeof(struct regulator_init_data), GFP_KERNEL);
		reg_config = kzalloc(sizeof(struct fixed_voltage_config), GFP_KERNEL);
		vreg_dev = kzalloc(sizeof(struct platform_device), GFP_KERNEL);
		dev_name = kzalloc(dev_name_size, GFP_KERNEL);

		snprintf(dev_name, dev_name_size, "%d-%04X", twi_id, twi_addr);
		max1363_supplies->dev_name = dev_name;
		max1363_supplies->supply = "vcc";

		reg_init_data->constraints.name = "adc_suply";
		reg_init_data->constraints.valid_ops_mask = REGULATOR_CHANGE_STATUS;
		reg_init_data->consumer_supplies = max1363_supplies;
		reg_init_data->num_consumer_supplies = 1;

		reg_config->supply_name = "adc_supply";
		reg_config->microvolts = supply_uv;
		reg_config->gpio = -EINVAL;
		reg_config->enabled_at_boot = 0;
		reg_config->init_data = reg_init_data;

		vreg_dev->name = "reg-fixed-voltage";
		vreg_dev->id = twi_addr;
		vreg_dev->num_resources = 0;
		vreg_dev->dev.platform_data = reg_config;

		status = platform_add_devices(&vreg_dev, 1);

		if (status) {
			printk("Failed to add regulator to %s with reason: %d", dev_name, status);
			kfree(dev_name);
			kfree(vreg_dev);
			kfree(reg_config);
			kfree(reg_init_data);
			kfree(max1363_supplies);
			continue;
		}

		adc_board_info = kzalloc(sizeof(struct i2c_board_info), GFP_KERNEL);
		strlcpy(adc_board_info->type, name, I2C_NAME_SIZE);
		adc_board_info->addr = twi_addr;

		status = i2c_register_board_info(twi_id, adc_board_info , 1);

		if (status) {
			kfree(adc_board_info);
		}
	}

	return status;
}

fs_initcall(max1363_board_init);

MODULE_DESCRIPTION("max1363 board");
MODULE_AUTHOR("Flames Group / Bohdan Shubenok");
MODULE_LICENSE("GPL");
