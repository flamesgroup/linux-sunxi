#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/mutex.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/regulator/machine.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <asm/byteorder.h>
#include <plat/sys_config.h>
#include <linux/i2c/at24.h>

static int __init at24_board_init(void)
{
	const struct sunxi_section *sp;
	struct i2c_board_info *at24_board_info;
	struct at24_platform_data *at24_platform_data;

	int status = 0;
	int eeprom_count = 0;
	char eeprom_para_name[32];

	int device_used = -1;
	char name[I2C_NAME_SIZE];
	int twi_id = -1;
	__u32 twi_addr = 0;
	int byte_len = -1;
	int page_size = -1;
	__u32 flags = 0;

	for (eeprom_count = 0; eeprom_count < 32; eeprom_count++) {
		snprintf(eeprom_para_name, sizeof(eeprom_para_name), "eeprom%d_para", eeprom_count);
		sp = sunxi_find_section(eeprom_para_name);
		if (!sp) {
			break;
		}
		pr_info("%s - read para section [%s]\n", __func__, eeprom_para_name);
		if (SCRIPT_PARSER_OK != script_parser_fetch(eeprom_para_name, "eeprom_used", &device_used, 1)) {
			pr_err("%s:%d - \"eeprom_used\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (device_used == 0) {
			continue;
		}
		memset(name, 0, sizeof(name));
		if (SCRIPT_PARSER_OK != script_parser_fetch(eeprom_para_name, "eeprom_name", (int *)name, sizeof(name))) {
			pr_err("%s:%d - \"eeprom_name\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(eeprom_para_name, "eeprom_twi_id", &twi_id, 1)) {
			pr_err("%s:%d - \"eeprom_twi_id\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(eeprom_para_name, "eeprom_twi_addr", &twi_addr, 1)) {
			pr_err("%s:%d - \"eeprom_twi_addr\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(eeprom_para_name, "eeprom_byte_len", &byte_len, 1)) {
			pr_err("%s:%d - \"eeprom_byte_len\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(eeprom_para_name, "eeprom_page_size", &page_size, 1)) {
			pr_err("%s:%d - \"eeprom_page_size\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}
		if (SCRIPT_PARSER_OK != script_parser_fetch(eeprom_para_name, "eeprom_flags", &flags, 1)) {
			pr_err("%s:%d - \"eeprom_flags\" script_parser_fetch err\n", __func__, __LINE__);
			continue;
		}

		at24_board_info = kzalloc(sizeof(struct i2c_board_info), GFP_KERNEL);
		at24_platform_data = kzalloc(sizeof(struct at24_platform_data), GFP_KERNEL);

		strlcpy(at24_board_info->type, name, I2C_NAME_SIZE);
		at24_board_info->addr = twi_addr;
		at24_board_info->platform_data = at24_platform_data;
		at24_platform_data->byte_len = byte_len;
		at24_platform_data->page_size = page_size;
		at24_platform_data->flags = flags;

		status = i2c_register_board_info(twi_id, at24_board_info, 1);
		if (status) {
			kzfree(at24_platform_data);
			kzfree(at24_board_info);
		}
	}
	return status;
}

fs_initcall(at24_board_init);

MODULE_DESCRIPTION("at24 board");
MODULE_AUTHOR("Flames Group / Ivan Metla");
MODULE_LICENSE("GPL");
